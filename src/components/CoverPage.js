import { createHeader, createMainComponent, renderFooter } from '../components';

const coverPage = () => {
    const page = document.createElement('div');
    page.classList.add('cover-container');
    page.classList.add('d-flex');
    page.classList.add('w-100');
    page.classList.add('h-100');
    page.classList.add('p-3');
    page.classList.add('mx-auto');
    page.classList.add('flex-column');

    page.appendChild(createHeader());
    page.appendChild(createMainComponent());
    page.appendChild(renderFooter());
    return page;
};

export { coverPage };
