import { callHomePage, callMenuPage, callContactPage } from './helpers';

const createNavbar = () => {
    const navbar = document.createElement('nav');
    navbar.classList.add('nav');
    navbar.classList.add('nav-masthead');
    navbar.classList.add('justify-content-center');
    // links
    // home
    let navLink = document.createElement('a');
    navLink.classList.add('nav-link');
    navLink.classList.add('active');
    navLink.href = '#';
    navLink.textContent = 'Home';
    navLink.addEventListener('click', callHomePage);
    navbar.appendChild(navLink);
    // menu
    navLink = document.createElement('a');
    navLink.href = '#';
    navLink.classList.add('nav-link');
    navLink.textContent = 'Menu';
    navLink.addEventListener('click', callMenuPage);
    navbar.appendChild(navLink);
    // contact
    navLink = document.createElement('a');
    navLink.href = '#';
    navLink.classList.add('nav-link');
    navLink.textContent = 'Contact';
    navLink.addEventListener('click', callContactPage);
    navbar.appendChild(navLink);
    return navbar;
};

const createMasthead = () => {
    // inner div
    const innerDiv = document.createElement('div');
    innerDiv.classList.add('inner');
    // create brand
    const innerH3 = document.createElement('h3');
    innerH3.classList.add('masthead-brand');
    innerH3.textContent = 'Restro';
    // append elements
    innerDiv.appendChild(innerH3);
    return innerDiv;
};

const createHeader = () => {
    const header = document.createElement('header');
    header.classList.add('masthead');
    header.classList.add('mb-auto');
    header.appendChild(createMasthead());
    header.appendChild(createNavbar());
    return header;
};

export { createHeader };
