const renderFooter = () => {
    const footer = document.createElement('footer');
    footer.classList.add('mastfoot');
    footer.classList.add('mt-auto');

    const innerDiv = document.createElement('div');
    innerDiv.classList.add('inner');

    const para = document.createElement('p');
    para.textContent = 'Developed by';
    const link = document.createElement('a');
    link.href = 'https://gitlab.com/vkainth';
    link.textContent = 'Vibhor Kainth';

    para.appendChild(link);
    innerDiv.appendChild(para);
    footer.appendChild(innerDiv);
    return footer;
};

export { renderFooter };
