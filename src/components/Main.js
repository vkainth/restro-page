const createMainComponent = () => {
    const main = document.createElement('main');
    main.classList.add('inner');
    main.classList.add('cover');
    main.setAttribute('role', 'main');
    const mainHeading = document.createElement('h1');
    mainHeading.classList.add('cover-heading');
    mainHeading.textContent = 'Desi Pizza';

    const mainSubtitle = document.createElement('p');
    mainSubtitle.classList.add('lead');
    mainSubtitle.textContent =
        'The only place you can get authentic Indian fusion. Come on over and live your fantasy of spending time in Italy while enjoying Indian cooking.';

    main.appendChild(mainHeading);
    main.appendChild(mainSubtitle);
    return main;
};

export { createMainComponent };
