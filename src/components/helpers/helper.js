const callHomePage = () => {
    console.log('Home Page Called');
    return true;
};

const callMenuPage = () => {
    console.log('Menu Page Called');
    return true;
};

const callContactPage = () => {
    console.log('Contact Page Called');
    return true;
};

export { callContactPage, callHomePage, callMenuPage };
