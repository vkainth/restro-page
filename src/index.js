import './style.css';
import { coverPage } from './components';
import {
    callContactPage,
    callMenuPage,
    callHomePage
} from './components/helpers';
// import bgImg from './bg-img.jpg';

const container = document.querySelector('#container');
if (callHomePage) {
    container.appendChild(coverPage());
}
